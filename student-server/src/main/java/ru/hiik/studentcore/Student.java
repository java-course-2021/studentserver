/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.studentcore;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author developer
 */
@Entity
@Table(name="students")
public class Student implements Serializable  {
    
   
    
    private String firstName;        // Имя 
    private String sureName;         // Фамилия
    private String middleName;       // Отчество
    private LocalDate birthDate;     // дата рождения
    private int yearOfStudy;         // Курс 
    private String studentGroup;     // Студенческая группа
    
    @Id
    @SequenceGenerator(name = "studentSeq", sequenceName = "student_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "studentSeq")
    private Long id;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
   
    @ManyToMany(fetch = FetchType.EAGER,mappedBy = "studentList") //FetchType.LAZY Ленивая инициализация(загрузка) - пока не вызову получение имени группы, ничего не выйдет
    private  Set<StudentGroup> studentsGroups = new HashSet<>();
    
    @Override
    public String toString( )
    {
        
        return "id ="+id +" "+sureName +" "+firstName+" "+middleName +" "+birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSureName() {
        return sureName;
    }

    public void setSureName(String sureName) {
        this.sureName = sureName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public String getStudentGroup() {
        return studentGroup;
    }

    public void setStudentGroup(String studentGroup) {
        this.studentGroup = studentGroup;
    }

    public Set<StudentGroup> getStudentsGroups() {
        return studentsGroups;
    }

    public void setStudentsGroups(Set<StudentGroup> studentsGroups) {
        this.studentsGroups = studentsGroups;
    }
    
    
}
