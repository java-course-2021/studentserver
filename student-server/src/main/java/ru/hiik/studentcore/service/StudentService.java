/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.studentcore.service;

import java.util.List;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import ru.hiik.studentcore.Student;


/**
 *  Класс, работающий с базой данных студентов
 * @author developer
 */
@ApplicationScoped
public class StudentService {
   
    @Inject
    EntityManager em; 
    
   /**
    * Получение полного списка студентов
    * @return 
    */ 
   @Transactional 
   public List<Student> getAll()
   {       
       
       
        List <Student> students = (List <Student>) em.createQuery("Select t from " + Student.class.getSimpleName() + " t").getResultList();
    
       
       return students;
   }
    
   /**
    * Сохранение студента в базе данных 
    * 
    * @param student
    * @return 
    */
   @Transactional
   public Student save (Student student)
   {
       em.persist(student);
      
       return student;
   }        
   
   @Transactional
   public long delete ( long id)
   {
       em.remove(id);
       return id;
   }        
   
   @Transactional
   public Optional<Student> find ( long id)
   {
       Optional<Student> opt = Optional.empty();
       
       Student  foundStudent = em.find(Student.class, id);
       if (foundStudent != null)
       {
           opt = Optional.of(foundStudent);
       }   
       
       return opt;
   }        
   
   
    
}
