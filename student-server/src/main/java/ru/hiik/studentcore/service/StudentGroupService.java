/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.hiik.studentcore.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.hibernate.Session;
import ru.hiik.studentcore.StudentGroup;

import org.jboss.logging.Logger;
import ru.hiik.studentcore.ApiResponse;
import ru.hiik.studentcore.Student;
/**
 *
 * @author dima
 */
@ApplicationScoped
public class StudentGroupService implements Serializable{
    
    private static final Logger log = Logger.getLogger(StudentGroupService.class);
    
    @Inject
    EntityManager em; 
    
    @Inject
    StudentService studentService;
    
    
   /**
    * Получение полного списка групп студентов
    * @return 
    */ 
   @Transactional 
   public List<StudentGroup> getAll()
   {       
       List <StudentGroup> studentGroups = (List <StudentGroup>) em.createQuery("Select t from " + StudentGroup.class.getSimpleName() + " t").getResultList();
       if (studentGroups.isEmpty())
       {
           studentGroups.stream().forEach( sg -> {
           log.info(String.format("%-25s %s", "["+sg.getName()+"]", ""+sg.getStudentList().size()+" студентов"));
           
           });
           
       }   
       
       return studentGroups;
   }
    
   /**
    * Сохранение группы в базе данных 
    * 
    * @param studentGroup
    * @return 
    */
   @Transactional
   public StudentGroup save (StudentGroup studentGroup)
   {
       em.persist(studentGroup);       
       return studentGroup;
   }        
   
   @Transactional
   public long delete ( long id)
   {
       em.remove(id);
       return id;
   }  
   
    @Transactional
    public ApiResponse addStudentToGroup(long student_id, long studentgroup_id) {
       
        log.info("Выполнение запроса на включение студента [" + student_id + "] в базу данных студентов и групп: [" + student_id + "]");
        ApiResponse response = new ApiResponse();
        log.info("Поиск студенческой группы с номером:  [" + studentgroup_id + "]...");
   
        StudentGroup foundGroup =  em.find(StudentGroup.class, studentgroup_id);
        if (foundGroup != null) {

            log.info("Обнаружена студенческая группа [" + foundGroup.getId() + " имя группы: " + foundGroup.getName() + "]");
            log.info("Поиск cтудента  с номером:  [" + student_id + "]...");
            
            Optional<Student> foundStudentOpt = studentService.find(student_id);
            if (foundStudentOpt.isPresent())
            {
                log.info("Обнаружен студент с номером:  [" + student_id + "]");
                foundGroup.addStudent(foundStudentOpt.get());
                log.info("Список студентов в группе [" + foundGroup.getId() + "] "+foundGroup.getStudentList().size()+" записей");
                log.info("Список студентов в группе [" + foundGroup.getId() + "] "+foundGroup.getStudentList().size()+" записей после обновления");
                em.persist(foundGroup);
            }   
            
        } else {
            log.error("Не найден группа ["+studentgroup_id+"]");
        }

        return response;

    }
    
}
