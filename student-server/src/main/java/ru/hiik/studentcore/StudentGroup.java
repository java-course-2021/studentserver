
package ru.hiik.studentcore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Класс представляющий список групп студентов 
 * 
 * @author vaganovdv
 */
@Entity(name = "StudentGroup")
@Table(name = "studentgroup")
public class StudentGroup   implements Serializable {
    
    private String name;                     // Название группы

    
    
    @ManyToMany( fetch = FetchType.EAGER )
    @JoinTable(
        name = "students_groups", 
        joinColumns = { @JoinColumn(name = "studentgroup_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "student_id") }
    )
    private  List<Student> studentList = new ArrayList<>();

    public void addStudent(Student student)
    {
        
        this.studentList.add(student);
        student.getStudentsGroups().add(this);
    }
            
    
    
    @Id
    @SequenceGenerator(name = "studentSeq", sequenceName = "student_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "studentSeq")
    private Long id;
    
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }


    
            
            
    
    
}
