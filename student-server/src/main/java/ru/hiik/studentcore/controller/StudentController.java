package ru.hiik.studentcore.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ru.hiik.studentcore.Student;
import ru.hiik.studentcore.StudentGroup;
import ru.hiik.studentcore.service.StudentService;

//   10.0.0.10:9797/students 
//   localhost:9797/students  

@Path("/students")
public class StudentController {

    private static final Logger log = Logger.getLogger(StudentController.class.getName());
    
    
    
    @Inject
    StudentService studentService;
    
      StudentGroup sg;
    /**
     * Получение полного списка студентов
     *
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)   // Преобразует список в JSON массив
    public List<Student> getAllStudent() {
        log.log(Level.INFO, "Поступил запрос на получение полного списка студентов");
        List<Student> students = studentService.getAll();
        if (students != null) {
            log.log(Level.INFO, "Получен список студентов [" + students.size() + "] записей");
        }

        return students;
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Student create(Student student) {
        log.log(Level.INFO, "Поступил запрос на сохранение студента в базе данных");
        Student savedStudent = studentService.save(student);
        if (savedStudent != null)
        {
            log.log(Level.INFO, "Студент: [" + savedStudent + "] сохранен в базе данных");
        }
        
        return savedStudent;
    }
   
    /**
     * Метод уадаления студента из базы данных 
     * 
     * @param student
     * @return 
     */
    @Path("/{id}")
    public long delete(@PathParam("id") long id)
    {  
        log.log(Level.INFO, "Поступил запрос на удаление студента из базы данных, идентификатор студента: ["+id+"]");
        long idDeletedStudent = studentService.delete(id); 
        log.log(Level.INFO, "Студент c номером : [" + idDeletedStudent + "] удален из базы данных");
        return idDeletedStudent;  
    }        
     
}