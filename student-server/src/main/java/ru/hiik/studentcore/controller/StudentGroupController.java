/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.hiik.studentcore.controller;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ru.hiik.studentcore.StudentGroup;
import ru.hiik.studentcore.service.StudentGroupService;
import org.jboss.logging.Logger;
import ru.hiik.studentcore.ApiResponse;

/**
 *
 * @author vaganovdv
 */

@Path("/student-groups")
public class StudentGroupController {

    private static final Logger log = Logger.getLogger(StudentController.class.getName());

    @Inject
    StudentGroupService studentGroupService;
    
    

    /**
     * Получение полного списка студентов
     *
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)   // Преобразует список в JSON массив
    public List<StudentGroup> getAllStudent() {
        log.info("Поступил запрос на получение полного списка студенческих групп");
        List<StudentGroup> groups = studentGroupService.getAll();
        if (groups != null) {
            log.info("Получен список студенческих групп [" + groups.size() + "] записей");
        }
        return groups;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public StudentGroup create(StudentGroup group) {
        log.info("Поступил запрос на сохранение студента в базе данных");
        StudentGroup savedGroup = studentGroupService.save(group);
        if (savedGroup != null) {
            log.info("Группа: [" + "id= " + savedGroup + " имя группы= " + savedGroup.getName() + "] сохранена в базе данных");
        } else {
            log.error("Группа: [ имя группы= " + savedGroup.getName() + "]  не сохранена в базе данных");
        }
        return savedGroup;
    }

    /**
     * Метод уадаления студента из базы данных
     *
     * @param student
     * @return
     */
    @Path("/{id}")
    @DELETE
    public long delete(@PathParam("id") long id) {
        log.info("Поступил запрос на удаление студента из базы данных, идентификатор студента: [" + id + "]");
        long idDeletedGroup = studentGroupService.delete(id);
        log.info("Группа c номером : [" + idDeletedGroup + "] удалена из базы данных");
        return idDeletedGroup;
    }

    @GET
    @Path("/add-student")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response  addStudentToGroup(@QueryParam(value = "studentId") @DefaultValue("0")  long studentId, @QueryParam(value = "studentGroupId") @DefaultValue("0")  long studentGroupId) {
       log.info("Поступил запрос на добавление студентa :" + studentId + " в группу " + studentGroupId);

       this.studentGroupService.addStudentToGroup(studentId, studentGroupId);
       ApiResponse response = new ApiResponse();
       response.setResult("OK");
       response.setDescription("Выполнено успешно");
       return  Response.ok(response).build();        
    }
}
